import React from 'react'
import { View, Text } from 'react-native'
import 'react-native-gesture-handler';
import MealsNavigator from "./src/navigation/MealsNavigator";
const App = () => {
  return (
    <View style={{flex:1}}>
      <MealsNavigator />

    </View>)
}
export default App