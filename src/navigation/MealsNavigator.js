import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from 'react-navigation';

import CatrgoriesScreen from "../screens/CategoriesScreen/CategoriesScreen"
import CategoryMealsScreen from "../screens/CategoryMealsScreen/CategoryMealsScreen";
import MealDetailScreen from "../screens/MealDetailScreen/MealDetailScreen";
import FavoritesScreen from "../screens/FavoritesScreen/FavoritesScreen";
import FilterScreen from "../screens/FilterScreen/FilterScreen";
const MealsNavigator= createStackNavigator({
    CatrgoriesScreen:CatrgoriesScreen,
    CategoryMealsScreen:CategoryMealsScreen,
    MealDetailScreen:MealDetailScreen,
    FavoritesScreen:FavoritesScreen,
    FilterScreen:FilterScreen
})

export default createAppContainer(MealsNavigator)